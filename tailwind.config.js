/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors:{
        'primary-green' : '#4DBB7A',
        'primary-orange' : '#EC5525',
        'secondary-green' : '#99F3BE',
      }
    },
  },
  plugins: [],
}
