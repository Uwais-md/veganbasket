import React from 'react'
// import Header from '../components/Header'
import Logo from '../assets/Logo.png'
import btnLogo from '/assets/button logo.png'
import Image from 'next/image'
import Hero from '../components/Hero'
import Intro from '@/components/Intro'
import Approach from '@/components/Approach'
import Footer from '@/components/Footer'
import Survey from '@/components/Survey'
import Launch from '@/components/Launch'


export default function Home() {
  const scrollToSection = (sectionId) => {
    const sectionElement = document.getElementById(sectionId);
    
    if (sectionElement) {
      sectionElement.scrollIntoView({ behavior: 'smooth' });
    }
  };
  return (

    <main className=' overflow-hidden max-w-max'>
      {/* <Header /> */}
      <div className=' px-1 sm:px-20 py-2 grid lg:grid-cols-5 grid-cols-2 items-center'>
            <div>
                <Image className='sm:w-[164px] sm:h-[135px] mb-2 w-[80px] h-[60px] mx-5' src={Logo} alt="" />
            </div>
            <div className='hidden col-span-3 lg:flex justify-center items-start gap-8 text-stone-800 mt-2 cursor-pointer'>
                <div className=' flex flex-col gap-1 items-center justify-center'  onClick={() => scrollToSection('intro')} >
                    <p>Who we are</p>
                    <div className=' h-[10px] w-10 bg-[#4DBB7A] rounded-lg'></div>
                </div>
                <button className=' cursor-pointer' onClick={() => scrollToSection('approach')}>
                    <p>Our Unique Approach</p>
                </button>
                <div className=' cursor-pointer'  onClick={() => scrollToSection('survey')}>
                    <p>How it Works</p>
                </div>
                <div className=' cursor-pointer'  onClick={() => scrollToSection('contact')}>       
                <p>Contact Us</p>
                </div>
            </div>
            <div className='flex justify-end'>
                <button className='flex items-center gap-2 bg-[#4DBB7A] text-[#fdfdfd] mr-4 sm:mr-0 font-medium text-lg py-2 sm:px-6 px-3 rounded '>
                    <Image className=' sm:w-[26px] w-5' src={btnLogo} alt="" />
                    <span className=' sm:text-md text-sm'>GET COUPONS</span>
                </button>
            </div>
        </div>
      <Hero id='hero'/>
      <Intro id='intro' />
      <Approach id='approach' />
      <Survey />
      <Launch />
      <Footer id='contact' />
    </main>
  )
}
