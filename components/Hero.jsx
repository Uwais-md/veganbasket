import React, { useState } from "react";
import { useSpring, animated } from 'react-spring';
import classNames from 'classnames';
import Image from "next/image";
import heroBg from "../assets/hero-bg.png";
import heroBg1 from "../assets/hero-bg1.png";
import couponImg from "../assets/coupon.png";
import man from "../assets/man.png";
import leaf from "../assets/leaf.png";
import apple from "../assets/apple.png";
import brinjal from "../assets/brinjal.png";
import carrot from "../assets/carrot.png";
import cauliflower from "../assets/cauliflower.png";
import watermelon from "../assets/watermelon.png";
import { useInView } from 'react-intersection-observer';


const Home = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  const fadeIn = useSpring({
    opacity: inView ? 1 : 0,
    from: { opacity: 0 },
    config: { duration: 1000 }, // Adjust the duration as needed
  });


  const animatedClass = classNames({
    'animate-fade-out': inView,
    // Add other classes as needed
  });

  return (
    <animated.div
      className={`sm:px-2 -ml-5 py-2 ${animatedClass}`}
      ref={ref}
      style={fadeIn}
    >
      <div
        className={`grid grid-cols-1 grid-rows-2 xl:grid-rows-1 xl:grid-cols-5 justify-between items-center py-20 ${animatedClass}`}
        ref={ref}
      >
        <div className="ml-0 sm:ml-14 order-2 lg:order-1 relative col-span-2 flex flex-col gap-8 items-center justify-center">
          <h1 className="relative flex flex-col gap-4 text-4xl sm:text-7xl sm:mt-0 mt-20 font-bold text-stone-800">
            <div>
              No <span className="text-[#EC5525]">Middleman</span>,
            </div>
            <div>Just Maximum</div>
            <div className="flex items-center gap-2 text-[#4DBB7A]">
              <span>Freshness</span>
              <Image src={leaf} alt="" />
            </div>
            <Image
              src={watermelon}
              className="absolute left-52 sm:left-72 -top-28 sm:w-28 sm:h-28 w-16 h-16"
              alt=""
            />
          </h1>
          <div>
            <button
              className="flex justify-start items-center gap-2 bg-primary-green ease-in-out text-[#fdfdfd] font-medium text-xl py-3 px-10 w-64 rounded hover:bg-primary-orange"
            >
              <Image className="w-[26px]" src={couponImg} alt="" />
              <span>GET COUPONS</span>
            </button>
          </div>
          <Image src={apple} className="absolute -left-36 -bottom-20" alt="" />
        </div>
        <div className="sm:mr-0 mr-10 lg:order-2 col-span-3 flex justify-center sm:ml-0 ml-14 items-center">
          <div className=" relative w-fit h-fit">
            <Image className=" w-[600px] " src={heroBg} alt="" />
            <div className=" absolute top-[16%] left-[25%]">
              <Image className=" w-[303px]" src={heroBg1} alt="" />
            </div>
            <Image
              className=" absolute -top-20 bottom-40 -left-1"
              src={man}
              alt=""
            />
            <div className=" absolute sm:top-96 sm:h-14 top-52 -right-2 bg-white shadow-md py-3 h-10 px-5 rounded-lg w-fit text-sm sm:text-lg font-semibold text-stone-700">
              FARMS TO YOUR FORK
            </div>
            <div className=" absolute sm:top-48 -right-10 top-14 sm:left-82 sm:text-lg sm:h-14 h-10 bg-white shadow-md py-3 px-5 rounded-lg text-sm sm:text-md w-fit font-semibold text-stone-700">
              15-20% LESS THAN MARKET PRICE
            </div>
            <div className=" absolute sm:top-64 top-36 right-30 sm:right-80 sm:h-14 sm:text-lg h-10 bg-white shadow-md py-3 px-5 rounded-lg text-sm sm:text-md w-fit font-semibold text-stone-700">
              DIRECT SOURCING
            </div>
            <Image src={apple} className=" absolute sm:top-2 -top-9 sm:left-6 sm:w-28 sm:h-28 w-16 h-16" alt="" />
            <Image src={brinjal} className=" absolute sm:top-2 -top-10 sm:-right-2 -right-1 sm:w-28 sm:h-28 w-16 h-16" alt="" />
            <Image
              src={carrot}
              className=" absolute bottom-20 -right-3 sm:w-28 sm:h-28 w-16 h-16"
              alt=""
            />
            <Image
              src={cauliflower}
              className=" absolute sm:bottom-56 top:44 sm:-left-16 sm:w-28 sm:h-28 w-16 h-16"
              alt=""
            />
          </div>
        </div>
      </div>
      <div className="flex justify-center items-center ml-14 sm:my-10">
        <div className=" bg-white shadow-lg py-3 px-4 md:px-8 rounded-md">
          <span className=" sm:text-xl text-md md:text-2xl my-3 pl-5 text-stone-800 font-semibold">
            Launching Our Mobile App Soon
          </span>
          <button className=" bg-[#EC5525] py-3 px-3 my-3 md:px-6 sm:text-md text-sm rounded-md font-semibold text-[#fdfdfd] ml-20 md:ml-11">
            NOTIFY ME
          </button>
        </div>
      </div>
    </animated.div>
  );
};

export default Home;
