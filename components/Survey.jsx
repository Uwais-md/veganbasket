import React from "react";
import Image from 'next/image'
import Work from '../assets/Frame 21.png'

const Survey = () => {
  return (
    
    <div className='flex flex-col items-center sm:m-10 ' id="survey">
      <Image src={Work} className="w-full h-full object-cover lg:px-20 lg:py-10 sm:px-10 sm:py-10 px-5 py-10"></Image>
    <div className='grid grid-cols-1 grid-rows-2 lg:grid-cols-2 lg:grid-rows-1 justify-center items-center gap-0 '>
      <div className=' sm:px-20 px-5 lg:w-auto'>
        <h1 className='my-5 font-bold text-4xl'>
          ENGAGE AND <span className='text-primary-green'>SAVE</span>
        </h1>
        <h2 className='my-5 font-bold text-lg'>
          Take Our Quick Survey for Exclusive Coupons
        </h2>
        <p className='my-5 font-light text-lg'>
          Your valuable insights help us continue to deliver the exceptional
          service you deserve.
        </p>
        <p className='my-5 font-light text-lg'>
          By participating in the survey, you're not only helping us, but
          you're also helping us
          <span className='font-bold ml-2 text-lg'>
            support local farmers and promote sustainable agriculture.
          </span>
        </p>
      </div>
      <div className='flex flex-col items-center justify-center md:w-[536px]'>
        <div className='sm:w-[536px] w-[300px] my-5 bg-slate-200 rounded-lg p-5'>
          <h1 className='sm:text-3xl text-xl font-bold my-5'>🌱 Shape Your Experience:</h1>
          <p>
            Your feedback helps us understand your needs and preferences,
            allowing us to curate vegan baskets that align with your tastes.
          </p>
        </div>
  
        <div className='my-5 bg-secondary-green sm:w-[536px] w-[300px] rounded-lg p-5'>
          <h1 className='sm:text-3xl text-xl font-bold my-5'>🌻 Enjoy Exclusive Savings</h1>
          <p>
            Your feedback helps us understand your needs and preferences,
            allowing us to curate vegan baskets that align with your tastes.
          </p>
        </div>
      </div>
    </div>
    <button className='bg-primary-green text-white px-5 py-3 rounded-md my-10 font-bold'>
      TAKE SURVEY NOW
    </button>
  </div>
  
  
  );
};

export default Survey;
