import React from 'react'
import sectionbg from "../assets/section.png"
import sectionMan from "../assets/section-man.png"
import Image from 'next/image'
import { useInView } from 'react-intersection-observer';
import { useSpring, animated } from 'react-spring';
import classNames from 'classnames';

const Section = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  const fadeIn = useSpring({
    opacity: inView ? 1 : 0,
    from: { opacity: 0 },
    config: { duration: 1000 }, // Adjust the duration as needed
  });


  const animatedClass = classNames({
    'animate-fade-out': inView,
    // Add other classes as needed
  });

  return (
    <animated.div
      className={`relative w-full  ${animatedClass}`}
      ref={ref}
      style={fadeIn}
    >

        <Image className=' w-full h-[1152px] object-cover' src={sectionbg} alt="" />
        <h1 className=' absolute top-14 left-[10%] sm:left-[15%] md:left-[20%] lg:left-[18%] xl:left-[25%] text-2xl sm:text-3xl md:text-4xl lg:text-5xl text-[#fdfdfd] font-bold'>WE LAUNCHING OUR APP SOON!</h1>
        <Image className=' absolute right-3 bottom-0 w-[700px] md:w-[1000px]' src={sectionMan} alt="" />
        <div className=' absolute left-5 sm:left-52 top-60 w-60 md:w-72 xl:w-80 bg-[#FCFCFD] shadow-sm rounded-lg py-6 px-6'>
            <h1 className=' text-stone-800 font-semibold text-xl md:text-2xl xl:text-3xl'>Seemless Shopping</h1>
            <p className=' mt-4 text-sm text-stone-700 md:text-base'>Explore our full range of vegtables and fruits and products effortlessly, and place your orders with a few taps.</p>
        </div>
        <div className=' z-10 absolute left-32 sm:left-80 top-[28rem] w-60 md:w-72 xl:w-80 bg-[#4DBB7A] shadow-sm rounded-lg py-6 px-6 border-2 border-white'>
            <h1 className=' text-stone-50 font-semibold text-xl md:text-2xl xl:text-3xl'>Seemless Shopping</h1>
            <p className=' mt-4 text-sm text-stone-100 md:text-base'>Explore our full range of vegtables and fruits and products effortlessly, and place your orders with a few taps.</p>
        </div>
        <div className=' absolute left-5 sm:left-52 top-[41.3rem] w-60 md:w-72 xl:w-80 bg-[#FCFCFD] shadow-sm rounded-lg py-6 px-6'>
            <h1 className=' text-stone-800 font-semibold text-xl md:text-2xl xl:text-3xl'>Seemless Shopping</h1>
            <p className=' mt-4 text-sm text-stone-700 md:text-base'>Explore our full range of vegtables and fruits and products effortlessly, and place your orders with a few taps.</p>
        </div>
        <button className=' absolute bottom-40 left-0 sm:left-40 lg:left-40 bg-[#EC5525] py-4 px-16 rounded-md font-semibold text-[#fdfdfd] ml-14'>NOTIFY ME</button>
        </animated.div>
  )
}

export default Section