import React from "react";
import Image from "next/image";
import Farmer from "../assets/farmer.png";
import Img1 from "../assets/recognize1.png";
import Img2 from "../assets/recognize2.png";
import IntroBG from "../assets/bg.png";
import { useInView } from 'react-intersection-observer';
import { useSpring, animated } from 'react-spring';
import classNames from 'classnames';


const Intro = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  const fadeIn = useSpring({
    opacity: inView ? 1 : 0,
    from: { opacity: 0 },
    config: { duration: 1000 }, // Adjust the duration as needed
  });


  const animatedClass = classNames({
    'animate-fade-out': inView,
    // Add other classes as needed
  });
  return (
    <animated.div
      id="intro"
      className={`w-full overflow-hidden${animatedClass}`}
      ref={ref}
      style={fadeIn}
    >
      <div>
        <div className={`relative w-full h-full`}>
          <Image
            src={IntroBG}
            alt="Bg"
            className="w-full h-[1500px] lg:h-[1000px] z-0 object-cover"
          />

          <div className=" absolute top-72 lg:top-20 bottom-10 flex flex-col items-center justify-center lg:flex-row">
            <div className=" z-50 w-1/2 text-white sm:text-lg text-md">
              <h1 className=" sm:text-5xl text-2xl font-bold">WHO WE ARE</h1>
              <p className=" mt-8 font-light">
                At Vegan Basket, where our passion for fresh, locally sourced
                vegan produce meets the convenience of urban living.
              </p>

              <p className=" mt-8 font-light">
                Founded with a vision to connect city dwellers with the essence
                of the farm, we're on a mission to redefine the way you
                experience your daily nourishment.
              </p>

              <p className=" mt-8 font-light">
                Our mission is simple yet impactful: connect you directly with
                farmers to bring you the finest, locally grown, and seasonal
                vegan products.
              </p>

              <p className=" mt-8 font-light">
                Say goodbye to the middleman, and say hello to affordable,
                top-quality produce.
              </p>

              <h3 className=" my-4 font-bold">Recognized by</h3>
              <Image src={Img1} alt="Startup" className=" sm:inline sm:mr-5 block my-5"></Image>
              <Image src={Img2} alt="Startup" className=" sm:inline sm:ml-5 block my-5"></Image>
            </div>

            <div className=" z-10">
              <Image
                src={Farmer}
                className=" w-52 sm:w-64 md:w-[400px] lg:w-[450px] xl:w-[600px] 2xl:w-full"
              ></Image>
              <div className=" sm:text-xl text-sm md:h-12 lg:text-2xl absolute lg:top-96 sm:bottom-52 sm:right-36 bottom-14 right-12 lg:right-24 bg-white shadow-md sm:py-3 py-2 px-2 sm:px-5  rounded-lg w-fit font-semibold text-stone-700">
                FRESH FROM FARM
              </div>
              <div className=" sm:text-xl text-sm md:h-12 lg:text-2xl absolute lg:bottom-48 -bottom-2 lg:right-46 bg-white shadow-md sm:py-3 py-2 px-2 sm:px-5 rounded-lg w-fit font-semibold text-stone-700">
                NO MIDDLE MAN
              </div>
            </div>
          </div>
        </div>
      </div>
      </animated.div>
  );
};

export default Intro;
